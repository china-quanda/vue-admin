/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/*
  校验手机号
**/
export function validMobile(str) {
  return /^1[3-9]\d{9}$/.test(str)
}

/*
  校验邮箱
**/
export function validEmail(str) {
  return /^\w+@[a-zA-Z0-9]{2,10}(?:\.[a-z]{2,4}){1,3}$/.test(str)
}

/*
  验证密码(密码只能由数字和字母组成)
**/
export function validPassword(str) {
  return /^[0-9A-Za-z]+$/.test(str)
}
/*
  密码必须包含数字和字母
**/
export function validPassword2(str) {
  return /^(?![^a-zA-Z]+$)(?!\D+$)/.test(str)
}
