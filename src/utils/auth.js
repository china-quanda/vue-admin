import Cookies from 'js-cookie'

const TokenKey = 'vue_admin_token'
const userInfo = 'userInfo'

const timeKey = 'vue_admin_token-time' // 用来作为时间戳存储的key

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setTimeStamp() {
  // 设置当前最新的时间戳
  // Date.now()  new Date.getTime()
  Cookies.set(timeKey, Date.now())
}

export function getTimeStamp() {
  return Cookies.get(timeKey)
}

export function getUserInfoo() {
  return JSON.parse(localStorage.getItem('userInfo'))
}
export function setUserInfoo(info) {
  return  localStorage.setItem(userInfo, JSON.stringify(info))
}
export function removeUserInfoo() {
  return localStorage.clear(userInfo)
}