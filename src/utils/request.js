import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getTimeStamp } from '@/utils/auth'
import router from '@/router'
const TimeOut = 3600000 * 2 // 定义token超时时间

// create an axios instance
const service = axios.create({
  // 设置基础路径 环境变量 npm run dev  /api   /生产环境 npm run build  /prod-api
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  timeout: 5000, // request timeout 超时时间 毫秒为单位
  // headers: {
  //   'Content-Type': 'application/json;charset=UTF-8',
  // },
})

// request interceptor 请求拦截器
service.interceptors.request.use(
  async config => {
    // 请求接口  config是请求配置
 // 取token // 在这个位置需要统一的去注入token
    if (store.getters.token) {
      // 只有在有token的情况下 才有必要去检查时间戳是否超时
      if (CheckIsTimeOut()) {
        // 如果它为true表示 过期了
        // token没用了 因为超时了
        await store.dispatch('user/logout')// 登出操作
        router.push(`/login`) // 跳到登录
        return Promise.reject(new Error('您的登录已失效，请重新登录'))
        // return Promise.reject(new Error('您的token已经失效'))
      }
      // 如果token存在 注入token
      config.headers.Authorization = `Bearer ${store.getters.token}`
    }
    return config//这里一定要注意 一定要return config
  },
  error => {
    // do something with request error
    console.log(error);
    return Promise.reject(error)
  }
)

// response interceptor 响应拦截器
service.interceptors.response.use(
  response => {
    // 成功执行
    // axios默认加了一层data的包裹
    const { code, message, data } = response.data
    if (code === 0) {
      // 此时认为业务执行成功了
      return data // 返回用户所需要的数据
    } else {
      // 当业务失败的时候 // 业务已经错误了 还能进then ? 不能 ！ 应该进catch
      Message.error(message) // 提示消息
      return Promise.reject(new Error(message))
    }
  }, async error => {
    // error 有response对象 config
    if (error.response && error.response.data && error.response.status === 401) {
      // 后端告诉前端token超时了
      await store.dispatch('user/logout') // 调用登出action
      router.push(`/login`)  // 跳到登录页
    }
    // 失败  Message等同于 this.$message
    Message.error(error.response.data.message) // 提示错误
    // Message.error(error.message) // 提示错误
    // reject
    return Promise.reject(error) // 传入一个错误的对象  就认为promise执行链 进入了catch
  })


// 检查token是否过期
// 是否超时
// 超时逻辑  (当前时间  - 缓存中的时间) 是否大于 时间差
function CheckIsTimeOut() {
  // 当前时间  - 存储的时间戳 > 时效性  false   tr
  return (Date.now() - getTimeStamp()) / 1000 > TimeOut
}
export default service
