import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css
// 导入阿里图标库
import '../src/assets/icon/iconfont.css'
// import '@/assets/icons/svg' // icon



import App from './App'
import store from './store'
import router from './router'
import Components from '@/components'

// 美化代码块
// import VueHighlightJS from 'vue-highlightjs'
// import 'highlight.js/styles/atom-one-dark.css'
// Vue.use(VueHighlightJS)

import '@/icons' // icon
import '@/permission' // permission control


// 注册全局过滤器
import * as filters from '@/filters'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 注册自定义指令 全局注册
import * as directives from '@/directives'
// 遍历所有的导出的指令对象 完成自定义全局注册
Object.keys(directives).forEach(key => {
  // 注册自定义指令
  Vue.directive(key, directives[key])
})
// 针对上面的引入语法  **`import *  as  变量`**  得到的是一个对象**`{ 变量1：对象1，变量2： 对象2 ...   }`**, 所以可以采用对象遍历的方法进行处理。
// 指令注册成功，可以在**`navbar.vue`**中直接使用了
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }
Vue.use(Components) // 注册自己的自定义组件

// Vue.use(ElementUI, { locale }) // set ElementUI lang to EN
Vue.use(ElementUI)// 如果想要中文版 element-ui

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
