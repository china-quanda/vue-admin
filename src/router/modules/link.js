// 导出属于员工的路由规则
import Layout from '@/layout'

export default {
  path: '/link', // 路径
  name: 'Link', // 给路由规则加一个name
  component: Layout, // 组件
  meta: {
    // title: '友情链接',
    // icon: 'user'
  },
  children: [
    {
      path: 'list',
      name: 'List',
      component: () => import('@/views/link'), 
      meta: { title: '友情链接' ,icon: 'user'},
      
    },
    // {
    //   path: 'userList',
    //   name: 'UserlList',
    //   component: () => import('@/views/user'), 
    //   meta: { title: '未定义' ,icon: ''},
      
    // }
  ]

}

// 
