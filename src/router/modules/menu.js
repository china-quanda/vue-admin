
import Layout from '@/layout'

export default {
  path: '/menu', 
  name: 'Menu', 
  component: Layout,
  meta: {
    // title: '菜单管理',
    // icon: 'el-icon-s-fold'
  },
  children: [
    {
      path: 'list',
      name: 'List',
      component: () => import('@/views/menu'),
      meta: { title: '菜单管理',icon: 'el-icon-s-fold' },
      
    }
  ]
  
}
