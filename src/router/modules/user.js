// 导出属于员工的路由规则
import Layout from '@/layout'

export default {
  path: '/user', // 路径
  name: 'User', // 给路由规则加一个name
  component: Layout, // 组件
  meta: {
    title: '用户管理',
    icon: 'user'
  },
  children: [
    {
      path: 'userList',
      name: 'UserlList',
      component: () => import('@/views/user'), 
      meta: { title: '用户列表' ,icon: 'user'},
      
    },
  ]
}

// 
