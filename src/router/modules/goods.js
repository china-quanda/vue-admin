// 导出属于员工的路由规则
import Layout from '@/layout'

export default {
  path: '/goods', 
  component: Layout, 
  meta: {
    title: '电商系统后台管理', 
    icon: 'el-icon-s-goods'
  },
  children: [{
    path: 'list', 
    component: () => import('@/views/goods/list'),
    meta: {
      title: '商品列表', 
      icon: 'el-icon-s-goods'
    }
  },
    {
      path: 'category',
      component: () => import('@/views/goods/category'),
      meta: {
        title: '类目管理', 
        icon: 'el-icon-s-goods'
      }
  },
  {
    path: 'brand',
    component: () => import('@/views/goods/Brand'),
    meta: {
      title: '品牌管理', 
      icon: 'el-icon-s-goods'
    }
},
{
  path: '/shop/list', 
  component: () => import('@/views/shop/ApplyShop'),
  meta: {
    title: '申请开店列表', 
    icon: 'el-icon-s-goods'
  }
}

]}

