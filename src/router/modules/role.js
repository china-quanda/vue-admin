// 导出属于员工的路由规则
import Layout from '@/layout'

export default {
  path: '/role', // 路径
  name: 'Role', // 给路由规则加一个name
  component: Layout, // 组件
  meta: {
    // title: '用户管理',
    // icon: 'el-icon-s-grid'
  },
  children: [
    {
      path: 'list',
      name: 'List',
      component: () => import('@/views/role'), 
      meta: { title: '角色列表' ,icon: 'el-icon-s-grid'},
      
    },
    // {
    //   path: 'userList',
    //   name: 'UserlList',
    //   component: () => import('@/views/user'), 
    //   meta: { title: '未定义' ,icon: ''},
      
    // }
  ]

}

// 
