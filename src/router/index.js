import Vue from 'vue'
import Router from 'vue-router'

import permissionRouter from './modules/permission'
import settingRouter from './modules/setting'
import goodsRouter from './modules/goods'
import articleRouter from './modules/article'
import userRouter from './modules/user'
import menuRouter from './modules/menu'
import roleRouter from './modules/role'
import linkRouter from './modules/link'



import news from '@/news/router'
import mall from '@/mall/router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'


// 静态路由
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
    meta: {
      title: '登录',
    }
  },
  {
    path: '/lockScreen',
    component: () => import('@/views/lockScreen'),
    hidden: true,
    meta: {
      title: '锁屏',
    }
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true,
    meta: {
      title: '404',
    }
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '首页', icon: 'dashboard' }
      },
    ]
  },
  {
    path: 'account',
    component: Layout,
    // redirect: '/account',
    children: [
      {
        path: '/account',
        name: 'Account',
        component: () => import('@/views/user/account'),
        hidden: true,
        meta: { title: '个人中心'}
      }
    ]
  },
 




  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
// 动态路由
export const asyncRoutes = [
  mall,
  news,
  articleRouter,
  goodsRouter,
  // menuRouter,
  // roleRouter,
  // userRouter,
  // departmentsRouter,
  // permissionRouter,
  // settingRouter,
  // linkRouter,
  
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  mode: 'hash', // 这里改为hash 之前是 history
  scrollBehavior: () => ({ y: 0 }),// 管理滚动行为 如果出现滚动 切换就让 让页面回到顶部
  routes: [...constantRoutes, ...asyncRoutes]// 临时合并所有的路由
})

const router = createRouter()


export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
