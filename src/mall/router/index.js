
import mallLayout from '@/mall/layout'

export default {
  
  path: '/mall', // 路径
  name: 'Mall', // 给路由规则加一个name
  redirect: '/mall/home',
  component: mallLayout, // 组件
  meta: { title:'商城前台系统'},
  children: [
    {
      path: 'home',
      name: 'MallHome',
      component: () => import('@/mall/views/home'),
      meta: { title:'商城首页'},
    },
    {
      path: '/mall/detail',
      name: 'MallDetail',
      component: () => import('@/mall/views/Detail'),
      meta: { title:'商品详情页'},
    },
    {
      path: '/mall/search',
      name: 'MallSearch',
      component: () => import('@/mall/views/Search'),
      meta: { title:'搜索'},
    },
    {
      path: '/mall/openShop',
      name: 'MallOpenShop',
      component: () => import('@/mall/views/OpenShop'),
      meta: { title:'开店'},
    },
    {
      path: '/mall/account',
      name: 'MallAccount',
      component: () => import('@/mall/views/Account'),
      meta: { title:'个人中心'},
    },
    {
      path: '/mall/favorites',
      name: 'MallFavorites',
      component: () => import('@/mall/views/Favorites'),
      meta: { title:'我的收藏夹'},
    },
    {
      path: '/mall/cart',
      name: 'MallCart',
      component: () => import('@/mall/views/Cart'),
      meta: { title:'购物车'},
    },
    {
      path: '/mall/footprint',
      name: 'MallFootprint',
      component: () => import('@/mall/views/Footprint'),
      meta: { title:'我的足迹'},
    },
    {
      path: '/mall/trade',
      name: 'MallTrade',
      component: () => import('@/mall/views/Trade'),
      meta: { title:'已买到的宝贝'},
    },
    {
      path: '/mall/login',
      name: 'MallLogin',
      component: () => import('@/mall/views/login'),
      meta: { title:'登录/注册'},
    },
    
  ],
 

}