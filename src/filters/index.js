import {formatTime} from '@/utils'
/**
 * 
 * @param {String} value 时间格式：2022-11-25-11:51
 * @returns 日期转换时间戳过滤器
 */
export const time = (value) => {
  return value ? formatTime(+new Date(value)) :'时间格式出错,请输入时间'
}


