
import newsLayout from '@/news/layout'

export default {

  path: '/news', // 路径
  name: 'News', // 给路由规则加一个name
  redirect: '/news/list',
  component: newsLayout, // 组件
  // hidden: true,
  meta: { title:'新闻前台系统'},
  children: [
    {
      path: 'list',
      name: 'NewsList',
      component: () => import('@/news/views/list'),
      hidden: true,
      meta: { title:'文章列表'},
    },
    {
      path: 'details/:article_id',
      name: 'NewsDetails',
      component: () => import('@/news/views/details'),
      hidden: true,
      meta: { title:'文章详情'},
    },
    {
      path: 'user/settings',
      name: 'NewsUserSettings',
      component: () => import('@/news/views/Settings'),
      hidden: true,
      meta: { title:'设置中心'},
    },
    {
      path: 'user/:user_id',
      name: 'NewsUser',
      component: () => import('@/news/views/User'),
      hidden: true,
      meta: { title:'个人中心'},
    },
    {
      path: 'creator',
      name: 'NewsCreator',
      redirect: '/news/creator/home',
      component: () => import('@/news/views/Creator'),
      hidden: true,
      meta: { title:'创作者中心'},
      children:[
        {
          path: 'home',
          component: () => import('@/news/views/Creator/Home'),
          hidden: true,
          meta: { title:'首页'},
        },
        {
          path: 'help/question',
          component: () => import('@/news/views/Creator/Help'),
          hidden: true,
          meta: { title:'常见问题'},
        },
        {
          path: 'article',
          component: () => import('@/news/views/Creator/Article'),
          hidden: true,
          meta: { title:'内容管理'},
        },
      ]
    },
   
  ],
 

}