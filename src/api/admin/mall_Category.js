import request from '@/utils/request'


// 获取类目列表
export const getCategoryList = params => request.get('/admin/mall/category',{params})

// 添加类目
export const addCategory = data=> request.post('/admin/mall/category',data)

// 修改类目信息
export const editCategory = (id,data)=> request.put(`/admin/mall/category/${id}`,data)

// 删除类目
export const deleteCategory = data => request.delete(`/admin/mall/category`,{data})