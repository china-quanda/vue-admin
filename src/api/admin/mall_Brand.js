import request from '@/utils/request'


// 获取类目列表
export const getBrandList = params => request.get('/admin/mall/brand',{params})

// 添加类目
export const addBrand = data=> request.post('/admin/mall/brand',data)

// 修改类目信息
export const editBrand = (id,data)=> request.put(`/admin/mall/brand/${id}`,data)

// 删除类目
export const deleteBrand = data => request.delete(`/admin/mall/brand`,{data})