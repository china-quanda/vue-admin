import request from '@/utils/request'



// 获取申请开店列表
export const getApplyShop = params => request.get('/admin/mall/shop/getApplyShop',{params})
// 根据id获取申请开店详细信息
export const getApplyShopId = (id) => request.get(`/admin/mall/shop/${id}`)
// 审核申请开店信息
export const auditApplyShop = (id,data) => request.put(`/admin/mall/shop/${id}`,data)

// 修改
// export const editUser = (id,data) => request.put(`/admin/user/${id}`,data)
