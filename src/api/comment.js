import request from '@/utils/request'




// 获取评论列表
export const getCommentList = params => request.get(`/admin/articlecomment/`,{params})

// 删除单个
// export const deleteRole = id => request.delete(`/admin/role/${id}`)

// 删除 可批量删除
export const deleteComment = data => request.delete(`/admin/articlecomment/`,{data})

// 添加
// export const addComment = data => request.post(`/admin/articlecomment/`,data)

// 修改
export const editCommen = (id,data) => request.put(`/admin/articlecomment/${id}`,data)

// 根据id获取
export const getCommenId = id => request.get(`/admin/articlecomment/${id}`)


// ================================================================

// // 对评论点赞
// export const addCommentLikings = data => request.post(`/admin/comment/likings`,data)
// // 取消对文章点赞
// export const deleteCommentLikings = id => request.delete(`/admin/comment/likings/${id}`)
// // 获取当前用户点赞过的全部信息
// export const getCommentLikingsList = () => request.get(`/admin/comment/likings`)

// ================================================================

// export const GetGoodsrelatedData = params => request.get('/goods/related', { params })
// export const GetGoodsCount = () => request.get('/cart/goodscount')