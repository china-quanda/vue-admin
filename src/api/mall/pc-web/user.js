import request from '@/utils/request'

const url = '/mall/pc-web/user';


// 获取用户邀请码
export const getUserInvitCode = () => request.get(`${url}/getUserInvitCode`)
// 使用方法：
// async getUserInvitCode(){
//   const code = await getUserInvitCode()
//   const origin = window.location.origin
//   const href =  origin+'/register'+code;
//   console.log(href);
// }
