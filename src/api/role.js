import request from '@/utils/request'


export function logout() {

}

// 获取角色列表
export const getRoleList = params => request.get('/admin/role',{params})

// 删除文章
// export const deleteRole = id => request.delete(`/admin/role/${id}`)

// 删除菜单 可批量删除
export const deleteRole = data => request.delete(`/admin/role/`,{data})

// 设置默认角色
export const setDefaultRole = id => request.patch(`/admin/role/setDefaultRole/${id}`)

// 添加角色
export const addRole = data => request.post(`/admin/role/`,data)

// 修改
export const editRole = (id,data) => request.put(`/admin/role/${id}`,data)

// 根据id获取角色
export const getRoleId = id => request.get(`/admin/role/${id}`)


// export const GetGoodsrelatedData = params => request.get('/goods/related', { params })
// export const GetGoodsCount = () => request.get('/cart/goodscount')