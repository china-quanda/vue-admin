import request from '@/utils/request'

// 获取文章列表
export const getArticleList = params => request.get('/admin/article',{params})

// 删除文章
// export const deleteArticle = id => request.delete(`/admin/article/${id}`)
export const deleteArticle = data => request.delete(`/admin/article/`,{data})


// 添加文章
export const addArticle = data => request.post(`/admin/article/`,data)

// 修改文章
export const editArticle = (id,data) => request.put(`/admin/article/${id}`,data)

// 根据id获取文章
export const getArticleId = id => request.get(`/admin/article/${id}`)


// ================================================================
// 获取举报文章列表
export const getAccusationArticleList = params => request.get(`/admin/article/reports`,{params})
// 根据id获取举报文章详细资料
export const getAccusationArticleInfo = id => request.get(`/admin/article/reports/${id}`)
// 根据id修改举报的文章
export const putAccusationArticleInfo = (id,data) => request.put(`/admin/article/reports/${id}`,data)
// 删除举报的文章
export const deleteAccusationArticle = data => request.delete(`/admin/article/reports`,{data})
// ================================================================


// 获取文章分类列表
export const getArticleCategoryList = params => request.get('/admin/articlecategory',{params})

// 删除文章分类
export const deleteArticleCategory = id => request.delete(`/admin/articlecategory/${id}`)