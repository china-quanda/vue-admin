import request from '@/utils/request'

// 获取友情链接列表
export const getLinkList = params => request.get('/admin/link',{params})
// 添加链接
export const addLink = data => request.post(`/admin/link/`,data)
// 删除友情链接
export const deleteLink = data => request.delete(`/admin/link/`,{data})
// 修改
export const editLink = (id,data) => request.put(`/admin/link/${id}`,data)
// 根据id获取友情链接
export const getLinkId = id => request.get(`/admin/link/${id}`)