import request from '@/utils/request'

// 获取菜单列表
export const getMenuList = params => request.get('/admin/menu',{params})

// 添加链接
export const addMenu = data => request.post(`/admin/menu/`,data)

// 删除菜单 可批量删除
export const deleteMenu = data => request.delete(`/admin/menu/`,{data})

// 修改
export const editMenu = (id,data) => request.put(`/admin/menu/${id}`,data)

// 根据id获取菜单
export const getMenuId = id => request.get(`/admin/menu/${id}`)