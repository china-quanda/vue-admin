import request from '@/utils/request'

// 获取商品列表
export const getGoodsList = params => request.get('/admin/mall/goods',{params})

// 删除商品
export const deleteGoods = id => request.delete(`/admin/mall/goods/${id}`)


// 获取商品分类列表
export const getGoodsCategoryList = params => request.get('/admin/mall/goodsCategory',{params})

// 删除商品分类
export const deleteGoodsCategory = id => request.delete(`/admin/mall/goodsCategory/${id}`)