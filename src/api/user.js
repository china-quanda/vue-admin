import request from '@/utils/request'

// 登录
export const login = data => request.post('/admin/user/login',data)

// 获取用户的基本资料
export const getUserInfo = id => {
  return request({
    url: `/admin/user/${id}`, // 因为所有的接口都要跨域 表示所有的接口要带 /api
    method: 'get',
  })
}

// 获取用户列表
export const getUserList = params => request.get('/admin/user',{params})

// 删除用户
export const deleteUser = data => request.delete(`/admin/user/`,{data})

// 添加用户
export const addUser = data => request.post(`/admin/user/`,data)

// 根据id获取用户信息
export const getUserId = id => request.get(`/admin/user/${id}`)

// 修改
export const editUser = (id,data) => request.put(`/admin/user/${id}`,data)
