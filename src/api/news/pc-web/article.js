import request from '@/utils/request'

// 获取文章列表 带用户搜索记录
export const getArticleList = params => request.get('/news/pc-web/article',{params})

// 根据文章id获取文章信息
export const getArticleId = id => request.get(`/news/pc-web/article/${id}`)

// 获取热门文章
export const getHotArticle = params => request.get('/news/pc-web/article/hot',{params})

// 举报文章
export const addReport = data => request.post(`/news/pc-web/article/report`,data)

// 获取用户举报文章列表 根据用户id
export const getUserReport = (id,params) => request.get(`/news/pc-web/article/report/userList/${id}`,{params})

// 删除举报的文章
export const deleteAccusationArticle = data => request.delete(`/news/pc-web/article/report`,{data})

// 对文章点赞
export const addArticleLikings = data => request.post(`/news/pc-web/article/liking`,data)

// 取消对文章点赞
export const deleteArticleLikings = id => request.delete(`/news/pc-web/article/liking/${id}`)

// 根据文章id获取点赞信息
export const getArticleLikingsId = id => request.get(`/news/pc-web/article/liking/${id}`)

// 收藏文章
export const addArticleCollections = data => request.post(`/news/pc-web/article/collect`,data)

// 取消收藏文章
export const deleteArticleCollections = id => request.delete(`/news/pc-web/article/collect/${id}`)

// 根据文章id判断是否已收藏文章
export const getArticleCollectionsId = id => request.get(`/news/pc-web/article/collect/${id}`)

// 获取用户浏览文章历史记录 根据用户id
export const getUserArticleViewHistory = (id,params) => request.get(`/news/pc-web/article/viewHistory/${id}`,{params})

// 获取用户收藏文章列表 根据用户id
export const getUserArticleCollections = (id,params) => request.get(`/news/pc-web/article/collect/${id}`,{params})

// 获取用户点赞文章列表 根据用户id
export const getUserArticleLikings = (id,params) => request.get(`/news/pc-web/article/liking/${id}`,{params})

// 获取用户发布的文章列表 根据用户id
export const getUserArticle = (id,params) => request.get(`/news/pc-web/article/userList/${id}`,{params})

