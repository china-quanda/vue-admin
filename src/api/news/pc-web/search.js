import request from '@/utils/request'

// 获取用户搜索的记录
export const getUserHistory = params => request.get('/news/pc-web/search/getUserHistory',{params})
// 获取热门搜索
export const getHotSearch = params => request.get('/news/pc-web/search/getHotSearch',{params})
// 删除单条用户搜索历史记录
export const delRowSearchHistory = id => request.delete(`/news/pc-web/search/delRowSearchHistory/${id}`)
// 删除用户所有搜索历史记录
export const delAllSearchHistory = () => request.delete(`/news/pc-web/search/delAllSearchHistory`)


