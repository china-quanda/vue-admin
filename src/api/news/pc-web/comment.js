import request from '@/utils/request'


// 获取评论列表
export const getCommentList = params => request.get(`/news/pc-web/comment`,{params})
// 添加
export const addComment = data => request.post(`/news/pc-web/comment`,data)

// 对评论点赞
export const addCommentLikings = data => request.post(`/news/pc-web/comment/liking`,data)

// 取消对文章点赞
export const deleteCommentLikings = id => request.delete(`/news/pc-web/comment/liking/${id}`)

// 获取当前用户点赞过的全部信息
export const getCommentLikingsList = () => request.get(`/news/pc-web/comment/liking`)

// 获取用户评论列表 根据用户id
export const getUserComment = (id,params) => request.get(`/news/pc-web/comment/userList/${id}`,{params})
