import request from '@/utils/request'


// 验证码快捷登录 或注册
export const quick = data => request.post(`/news/pc-web/user/quick`,data)

// 账号密码登录
export const login = data => request.post(`/news/pc-web/user/login`,data)

// 验证码快捷修改密码
export const quickUpdatePassword = data => request.patch(`/news/pc-web/user/quickUpdatePassword`,data)

// gitHub登录
export const gitHubLogin = data => request.post(`/news/pc-web/user/gitHubLogin`,data)

// 微信登录
export const weChatLogin = data => request.post(`/news/pc-web/user/weChatLogin`,data)


// 关注文章作者
export const addUserFollowings = data => request.post(`/news/pc-web/user/focus`,data)

// 取消关注文章作者
export const deleteUserFollowings = id => request.delete(`/news/pc-web/user/focus/${id}`)

// 根据文章作者user_id获取关注信息
export const isWage = id => request.get(`/news/pc-web/user/focus/${id}`)

// 获取用户粉丝列表 根据用户id
export const getUserFollowers = (id,params) => request.get(`/news/pc-web/user/followers/${id}`,{params})

// 获取用户关注列表 根据用户id
export const getUserFollowings = (id,params) => request.get(`/news/pc-web/user/followings/${id}`,{params})

// 获取用户主页个人信息
export const getUserHomePage = id => request.get(`/news/pc-web/user/HomePage/${id}`)
