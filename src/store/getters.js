const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,// 在根级的getters上 开发子模块的属性给别人看 给别人用
  avatar: state => state.user.userInfo.avatar,
  nickname: state => state.user.userInfo.nickname ,// 建立用户名称的映射
  // name: state => state.user.userInfo.nickname ,// 建立用户名称的映射 等删除
  userId: state => state.user.userInfo.id // 建立用户id的映射
}
export default getters
