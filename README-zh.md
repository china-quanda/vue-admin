前端技术：Vue + Element-UI + Axios

```
// 前端项目地址：https://gitee.com/china-quanda/vue-admin
```

后端技术：Node.js + Egg企业级应用框架 + Mysql数据库 + Sequelize数据库ORM框架

```
// 后端项目地址：https://gitee.com/china-quanda/api-egg-mysql-sever
```
登录页面
![登录页面](git_img/login.png)
锁屏页面
![锁屏页面](git_img/%E9%94%81%E5%B1%8F%E9%A1%B5%E9%9D%A2.png)
登录滑块验证
![登录滑块验证](git_img/%E7%99%BB%E5%BD%95%E6%BB%91%E5%8A%A8%E9%AA%8C%E8%AF%81.png)
友情链接页面
![友情链接](git_img/%E5%8F%8B%E6%83%85%E9%93%BE%E6%8E%A5.png)
个人中心-账号绑定
![个人中心-账号绑定](git_img/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83-%E8%B4%A6%E5%8F%B7%E7%BB%91%E5%AE%9A.png)
个人中心-安全设置
![个人中心-安全设置](git_img/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83-%E5%AE%89%E5%85%A8%E8%AE%BE%E7%BD%AE.png)
文章系统-文章详情页
![文章详情页](git_img/文章详情页.png)
文章系统-文章列表页
![文章列表页](git_img/文章列表页.png)

## Build Setup

```bash
# 克隆项目
git clone https://gitee.com/china-quanda/vue-admin.git

# 进入项目目录
cd vue-admin

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:8888](http://localhost:8888)

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```

